CREATE OR REPLACE FUNCTION version_employee()
RETURNS TRIGGER AS
$employee_ver$
BEGIN
IF (TG_OP == 'UPDATE') THEN
    UPDATE hrm.employee
    SET position_valid_to_dt = current_date
    WHERE employee_id = old.employee_id;

    INSERT INTO hrm.employee VALUES (old.employee_id, new.employee_nm, new.position,
        current_date, 9999-01-01, new.salary_value, new.department_id);

    RETURN NEW;
END IF;
END;
$employee_ver$
LANGUAGE plpgsql;


CREATE OR REPLACE TRIGGER employee_ver
    BEFORE UPDATE ON hrm.employee
    FOR EACH ROW
    EXECUTE FUNCTION version_employee();


CREATE OR REPLACE FUNCTION clean_candidate()
RETURNS TRIGGER AS
$clean_candidate$
DECLARE id integer;
BEGIN

DELETE FROM hrm.candidate WHERE candidate.candidate_nm ILIKE new.employee_nm
    RETURNING candidate_id INTO id;

DELETE FROM hrm."candidateContacts" WHERE candidate_id = id;
DELETE FROM hrm."candidateVacancyList" WHERE candidate_id = id;

RETURN new;
END;
$clean_candidate$
LANGUAGE plpgsql;

CREATE OR REPLACE TRIGGER candidate_cleaning
    AFTER INSERT ON hrm.employee
    EXECUTE FUNCTION clean_candidate();




