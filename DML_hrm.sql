INSERT INTO hrm.vacancy
    (vacancy_id, vacancy_nm, vacancy_valid_from_dttm, vacancy_valid_to_dttm, vacancy_salary_value, department_id)
VALUES (11, 'Junior Java Developer', now(), '2024-01-01 23:59:59+03', 150000, 5);

INSERT INTO hrm.candidate
    (candidate_id, candidate_nm, candidate_cv_url)
VALUES (11, 'Анатолий Геннадьевич Тиджой', 'https//:data.org/cv/tijoe.pdf');

DELETE FROM hrm."candidateVacancyList"
WHERE candidate_id = '5';

SELECT *
FROM hrm.employee;

SELECT *
FROM hrm."candidateVacancyList"
LIMIT 5;

SELECT *
FROM hrm.employee
WHERE employee.salary_value > 150000;

SELECT employee_nm
FROM hrm.employee
WHERE salary_value BETWEEN 100000 AND 210000;

UPDATE hrm.employee
SET salary_value = salary_value + 20000; --субсидия от государства для работников компании

UPDATE hrm.education
SET tutor = 'Линус Бенедикт Торвальдс'
WHERE tutor = 'Михаил Петрович Зубенко';

UPDATE hrm.education
SET info_url = 'https//:data.org/edu/linux.pdf'
WHERE tutor = 'Линус Бенедикт Торвальдс';

