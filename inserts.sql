--fill candidate
INSERT INTO hrm.candidate (candidate_id, candidate_nm, candidate_cv_url)
VALUES (1, 'Василий Васильевич Пупкин', 'https//:data.org/cv/pupkin.pdf'),
       (2, 'Константин Сергеевич Пушкин', 'https//:data.org/cv/pushkin.pdf'),
       (3, 'Михаил Петрович Зубенко', 'https//:data.org/cv/mafioznik.pdf'),
       (4, 'Владислав Андреевич Бумага', 'https//:data.org/cv/bumaga.pdf'),
       (5, 'Линус Бенедикт Торвальдс', 'https//:data.org/cv/linus.pdf'),
       (6, 'Михаил Андреевич Хорев', 'https//:data.org/cv/khorev.pdf'),
       (7, 'Александр Анатольевичн Вагнер', 'https//:data.org/cv/vagner.pdf'),
       (8, 'Юрий Олегович Фадеев', 'https//:data.org/cv/fadeev.pdf'),
       (9, 'Арсений Витальевич Занозин', 'https//:data.org/cv/zanozin.pdf'),
       (10, 'Олег Германович Седов', 'https//:data.org/cv/sedov.pdf');

--fill department
INSERT INTO hrm.department (department_id, department_nm, director)
VALUES (1, 'Москва-1', 'Владимир Алексеевич Вавилов'),
       (2, 'Москва-2', 'Александра Константиновна Жукова'),
       (3, 'Новосибирск-1', 'Илья Борисович Литвинов'),
       (4, 'СПБ-1', 'Иван Маркович Воронов'),
       (5, 'СПБ-2', 'Милана Марковна Григорьева'),
       (6, 'Махачкала-1', 'Магомед Нурбагандович Исмаилов'),
       (7, 'Ярославль-1', 'Анатолий Иванович Вагин'),
       (8, 'Саратов-1', 'Ярослав Григорьевич Ильин'),
       (9, 'Мухосранск-1', 'Виктор Подпивкович Пятничный'),
       (10, 'Казань-1', 'Вероника Петровна Крылова');

--fill employee
INSERT INTO hrm.employee
    (employee_id, employee_nm, position, position_valid_from_dt, position_valid_to_dt, salary_value, department_id)
VALUES (1, 'Марина Максимовна Новикова', 'System Analyst', '2021-07-26', null, 200000, 1),
       (2, 'Надежда Михайловна Ковалева', 'Data Scientist', '2021-01-18', '2023-01-18', 300000, 3),
       (3, 'Али Даниилович Бирюков', 'Information Security Architect', '2022-08-13', null, 150000, 4),
       (4, 'Владимир Павлович Воробьев', 'DevOps Engineer', '2022-08-13', '2023-04-28', 250000, 1),
       (5, 'Антонина Александровна Кузнецова', 'Frontend Developer', '2020-04-17', null, 777600000000000000, 5),
       (6, 'Глеб Максимович Сорокин', 'Senior Java Developer', '2020-09-28', null, 200000,7),
       (7, 'Тимур Матвеевич Зайцев', 'Senior Java Developer', '2020-02-24', null, 200000, 1),
       (8, 'Александр Алиевич Петухов', 'Junior Java Developer', '2021-10-15', null, 100000,1),
       (9, 'Тигран Георгиевич Трифонов', 'Business Analyst', '2023-11-29', null, 250000,1),
       (10, 'Владислав Адамович Малинин', 'HR manager', '2019-11-02', null, 160000, 10);

--fill vacancy
INSERT INTO hrm.vacancy
    (vacancy_id, vacancy_nm, vacancy_valid_from_dttm, vacancy_valid_to_dttm, vacancy_salary_value, department_id)
VALUES (1, 'System Analyst', '2023-01-15 00:00:00+03', '2024-01-01 23:59:59+03', 150000, 1),
       (2, 'System Analyst', '2023-04-11 00:00:00+03', '2024-01-01 23:59:59+03', 170000, 3),
       (3, 'Frontend Developer', '2023-06-19 00:00:00+03', '2024-01-01 23:59:59+03', 100000, 4),
       (4, 'Android Developer', '2023-05-28 00:00:00+03', '2024-01-01 23:59:59+03', 200000, 1),
       (5, 'DevOps Engineer', '2023-01-15 00:00:00+03', '2024-01-01 23:59:59+03', 200000, 1),
       (6, 'Junior Java Developer', '2023-03-07 00:00:00+03', '2024-01-01 23:59:59+03', 150000, 5),
       (7, 'Junior Java Developer', '2023-02-13 00:00:00+03', '2024-01-01 23:59:59+03', 150000, 6),
       (8, 'Senior Java Developer', '2023-03-11 00:00:00+03', '2024-01-01 23:59:59+03', 270000, 1),
       (9, 'HR manager', '2023-04-23 00:00:00+03', '2024-01-01 23:59:59+03', 180000, 3),
       (10, 'Data Scientist', '2023-01-15 00:00:00+03', '2024-01-01 00:00:00+03', 300000, 7);

--fill education
INSERT INTO hrm.education (education_id, education_nm, info_url, tutor, education_valid_from_dt, education_valid_to_dt)
VALUES (1, 'DB course', 'https//:data.org/edu/db.pdf', 'А. М. Рыбаков', '2023-02-01', '2023-02-28'),
       (2, 'Concurency', 'https//:data.org/edu/concurncy.pdf', 'А. Д. Ульянов', '2023-03-01', '2023-03-31'),
       (3, 'Kotlin', 'https//:data.org/edu/kotlin.pdf', 'В. Н. Колосова', '2023-04-01', '2023-04-07'),
       (4, 'System optimization', 'https//:data.org/edu/optim.pdf', 'Д. Г. Шевелев', '2023-09-01', '2023-09-30'),
       (5, 'Java', 'https//:data.org/edu/java.pdf', 'В. А. Максимова', '2023-04-08', '2023-04-14'),
       (6, 'Linux', 'https//:data.org/edu/linux_mafia.pdf', 'М. П. Зубенко', '2023-02-01', '2023-04-30'),
       (7, 'SpringBoot Java', 'https//:data.org/edu/spring-boot.pdf', 'В. А. Максимова', '2023-04-01', '2023-04-14'),
       (8, 'Business strategy course','https//:data.org/edu/business-strat.pdf', 'Р. Р. Кузнецов', '2023-07-01', '2023-07-07'),
       (9,  'GameDev', 'https//:data.org/edu/game-dev.pdf', 'В. А. Морозов', '2023-02-01', '2023-05-31'),
       (10, 'UnrealEngine5', 'https//:data.org/edu/ue5.pdf', 'В. А. Морозов', '2023-06-01', '2023-08-30');

--fill candidateContacts
INSERT INTO hrm."candidateContacts" (candidate_id, mail, phone_no, telegram, adress)
VALUES (1, 'pupkin.va@mail.ru', '8(936)340-49-70', '@pupkin1977', '998253, Иркутская обл., г. Можайск, пр-д Домодедовская, 24'),
       (2, 'pushkin.k@mail.ru', '8(411)365-40-44', null, '231267, Самарская обл., г. Щёлково, ул. Гоголя, 91'),
       (3, 'mik.zubenko@gmail.com', '8(936)340-49-70', '@maFioZnik', null),
       (4, 'bumaga@mail.ru', '8(936)340-49-70', '@bumaga_A4', '939348, Белгородская обл., г. Клин, пер. 1905 года, 83'),
       (5, 'linus.torwalds@gmail.com', '8(936)340-49-70', null, '916762, Орловская обл., г. Мытищи, пер. Будапештсткая, 97'),
       (6, 'khorev.m@yandex.ru', '8(936)340-49-70', null, '624691, Свердловская обл., г. Павловский Посад, пр-д Балканская, 18'),
       (7, 'vagner.a@mail.ru', '8(936)340-49-70', '@vagner228', '758198, Саратовская обл., г. Наро-Фоминск, пр-д Сталина, 66'),
       (8, 'fadeev.yura@mail.ru', '8(936)340-49-70', null, null),
       (9, 'zanoza.tree@yandex.ru', '8(936)340-49-70', '@doska_zanoza', '150921, Кемеровская обл., г. Зарайск, пл. Космонавтов, 27'),
       (10, 'geography@gmail.com', '8(936)340-49-70', '@sedov1971', '722378, Амурская обл., г. Озёры, наб. 1905 года, 53');

--fill candidateVacancyList
INSERT INTO hrm."candidateVacancyList" (candidate_id, vacancy_id)
VALUES (1, 9),
       (1, 1),
       (1, 8),
       (2, 1),
       (3, 2),
       (4, 5),
       (6, 4),
       (7, 8),
       (7, 1),
       (9, 7);

--fill employeeContacts
INSERT INTO hrm."employeeContacts" (employee_id, mail, phone_no, telegram, adress)
VALUES (1, 'm.novikova@mail.ru', '8(095)421-84-52', '@novikova1987', 'Гагаринский пер., 34, Москва, 195326'),
       (2, 'n.kovaleva@mail.ru', '8(534)784-03-39', '@kovalev129', 'Таганская ул., 40, Москва, 173364'),
       (3, 'a.biryukov@gmail.com', '8(095)253-84-72', '@biryukov', 'Гагаринский пер., 65, Москва, 166027'),
       (4, 'v.vorobyov@mail.ru', '8(264)627-89-10', '@vorobyov_228', 'Мясницкая ул., 15, Москва, 13380'),
       (5, 'a.kuznetsova@gmail.com', '8(306)345-17-46', '@forge_a', 'Малый Черкасский пер., 29, Москва, 197015'),
       (6, 'g.sorokin@yandex.ru', '8(801)715-36-30', 'boltay_gold', 'Конная ул., 37, лит.А, Санкт-Петербург, 191418'),
       (7, 't.zaytsev@mail.ru', '8(614)278-51-60', '@zaytsev', 'ул. Марата, 81, Санкт-Петербург, 191136'),
       (8, 'a.petuhov@mail.ru', '8(376)125-55-09', '@cockEREL', null),
       (9, 't.trifonov@yandex.ru', '8(694)181-95-60', '@3fonov', 'Чернорецкий пер., 86, Санкт-Петербург, 191808'),
       (10, 'v.malinin@gmail.com', '8(328)070-18-89', '@yagodka', 'ул. Бориса Богаткова, 93, корп. 1, Новосибирск, Новосибирская обл., 630030');

--fill vacation
INSERT INTO hrm.vacation (vacation_id, employee_id, vacation_reasons_url, vacation_valid_from_dt, vacation_valid_to_dt)
VALUES (1, 10, 'https//:data.org/rest/reason-malinin.pdf', '2023-10-10', '2023-10-24'),
       (2, 9 , 'https//:data.org/rest/reason-trifonov.pdf', '2023-07-10', '2023-07-24'),
       (3, 8, 'https//:data.org/rest/reason-petuhov.pdf', '2023-08-01', '2023-08-15'),
       (4, 7, 'https//:data.org/rest/reason-zaytsev.pdf', '2023-06-10', '2023-07-10'),
       (5, 6, 'https//:data.org/rest/reason-sorokin.pdf', '2023-04-10', '2023-04-24'),
       (6, 5, 'https//:data.org/rest/reason-kuznetsova.pdf', '2023-01-14', '2023-01-28'),
       (7, 5, 'https//:data.org/rest/decree-kuznetsova.pdf', '2023-04-10', '2024-08-24'),
       (8, 3, 'https//:data.org/rest/reason-biryukov.pdf', '2023-10-10', '2023-10-24'),
       (9, 4, 'https//:data.org/rest/reason-vorobyov.pdf', '2023-02-10', '2023-02-24'),
       (10, 2, 'https//:data.org/rest/decree-kovaleva.pdf', '2023-04-10', '2024-08-24');

--fill educationParticipantsList
INSERT INTO hrm."educationParticipantsList" (education_id, employee_id)
VALUES (1, 1),
       (2, 1),
       (2, 3),
       (5, 5),
       (6, 8),
       (7, 9),
       (8, 8),
       (9, 1),
       (9, 4),
       (10, 4);