--Все разработчики в компании
SELECT employee_nm,
       position,
       salary_value
FROM hrm.employee
WHERE position ILIKE '%developer'
ORDER BY salary_value DESC;

--Список сотрудников с должностями в Московских депратаменте
SELECT department_nm,
       employee_nm,
       position
FROM hrm.employee e
INNER JOIN hrm.department d ON e.department_id = d.department_id
WHERE department_nm ILIKE 'москва%'
ORDER BY employee_nm;

--Список кандидатов с сылками на их резюме и перечнем имён вакансий,
--на которые они претендуют
WITH t AS (
    SELECT c.candidate_id AS candidate_id,
           c.candidate_nm AS candidate_nm,
           c.candidate_cv_url AS candidate_cv,
           cc.mail AS mail,
           cc.phone_no AS phone_no,
           COALESCE(cvl.vacancy_id, 0) AS vacancy_id,
           vac.vacancy_nm
    FROM hrm.candidate AS c
    LEFT JOIN hrm."candidateContacts" AS cc on c.candidate_id = cc.candidate_id
    LEFT JOIN hrm."candidateVacancyList" AS cvl on c.candidate_id = cvl.candidate_id
    LEFT JOIN hrm.vacancy AS vac on vac.vacancy_id = cvl.vacancy_id
)

SELECT
    candidate_nm,
    candidate_cv,
    array_to_string(array_agg(DISTINCT vacancy_nm ORDER BY vacancy_nm DESC), ', ') AS vacancies
FROM t
GROUP BY candidate_nm, candidate_cv
HAVING array_to_string(array_agg(DISTINCT vacancy_id), ', ') NOT ILIKE '%0%'
ORDER BY candidate_nm;

--Данные о средней зарплате и занчении её медианы по городам,
--в которых есть филиалы компании
WITH t as (
    SELECT split_part(dep.department_nm, '-', 1) AS city,
           emp.salary_value AS salary
    FROM hrm.employee AS emp
        LEFT JOIN hrm.department AS dep on emp.department_id = dep.department_id
    ORDER BY department_nm
)

SELECT t.city,
       avg(t.salary) AS avg_salaty,
       percentile_disc(0.5)
           WITHIN GROUP ( ORDER BY t.salary)
FROM t
GROUP BY t.city
ORDER BY t.city;

--Список крусов с информацией о них и перечнем участников
WITH t AS (
    SELECT edu.education_nm,
           edu.info_url,
           edu.tutor,
           e.employee_nm,
           e.position
    FROM hrm.education AS edu
    LEFT JOIN hrm."educationParticipantsList" ePL on edu.education_id = ePL.education_id
    LEFT JOIN hrm.employee AS e on ePL.employee_id = e.employee_id
)

SELECT
    education_nm,
    tutor,
    info_url,
    array_to_string(array_agg(DISTINCT employee_nm ORDER BY employee_nm), E'\n')
FROM t
GROUP BY education_nm, tutor, info_url
HAVING array_to_string(array_agg(DISTINCT employee_nm ORDER BY employee_nm), E'\n') NOT ILIKE ''
ORDER BY education_nm;

--Компания в кризисном состоянии. Надо понять, какие отделы компании пойдут под сокращение.
--Список отделов компаниями c информацией о зарплатах сотрудникам и процентным ранжированием.
WITH t1 AS (
    SELECT split_part(department_nm, '-', 1) AS city,
           department_nm,
           count(employee_nm) AS number_of_employee,
           sum(salary_value) AS expenses
    FROM hrm.department AS dep
    LEFT JOIN hrm.employee emp on dep.department_id = emp.department_id
    GROUP BY department_nm
    ORDER BY city
), t2 AS (
    SELECT *,
           (expenses / number_of_employee) AS expenses_per_employee
    FROM t1
)

SELECT city,
       department_nm,
       number_of_employee,
       expenses,
       expenses_per_employee,
       cume_dist() over (order by expenses_per_employee DESC)
FROM t2
WHERE number_of_employee <> 0




