CREATE OR REPLACE VIEW moscow_employees AS
    SELECT department_nm,
       employee_nm,
       position
    FROM hrm.employee e
    INNER JOIN hrm.department d ON e.department_id = d.department_id
    WHERE department_nm ILIKE 'москва%'
    ORDER BY employee_nm;


CREATE OR REPLACE VIEW edu_list AS
    SELECT education_nm,
           info_url,
           education_valid_from_dt
    FROM hrm.education edu
    WHERE education_valid_from_dt > now()::date;


CREATE OR REPLACE VIEW department_employees_count AS
    SELECT department_nm,
           count(e.employee_id) as employees_cnt
    FROM hrm.employee e
    INNER JOIN hrm.department d ON e.department_id = d.department_id
    GROUP BY department_nm;


CREATE OR REPLACE VIEW employee_phone AS
    SELECT employee_nm,
           position,
           concat(split_part(phone_no, ')', 1), ')******', split_part(phone_no, '-', 3)) as phone
    FROM hrm.employee e
    INNER JOIN hrm."employeeContacts" eC on e.employee_id = eC.employee_id;


CREATE OR REPLACE VIEW candidate_phone AS
    SELECT candidate_nm,
           candidate_cv_url,
           concat(split_part(phone_no, ')', 1), ')******', split_part(phone_no, '-', 3)) as phone
    FROM hrm.candidate c
    INNER JOIN hrm."candidateContacts" cC on c.candidate_id = cC.candidate_id;


CREATE OR REPLACE VIEW avg_salary_in_diff_cities AS

    WITH t as (SELECT split_part(dep.department_nm, '-', 1) AS city,
                      emp.salary_value                      AS salary
               FROM hrm.employee AS emp
                        LEFT JOIN hrm.department AS dep on emp.department_id = dep.department_id
               ORDER BY department_nm)

    SELECT t.city,
       avg(t.salary) AS avg_salaty,
       percentile_disc(0.5)
           WITHIN GROUP ( ORDER BY t.salary)
    FROM t
    GROUP BY t.city
    ORDER BY t.city;





